package com.xuexuehai.BQMS.service;

import com.xuexuehai.BQMS.dao.Book;
import com.xuexuehai.BQMS.exception.CustomizeErrorCode;
import com.xuexuehai.BQMS.exception.CustomizeException;

/**
 * BookQueryMSService实现了系统的各项逻辑功能
 *
 * @Author: xuexuehai
 * @MailBox: xuehai.xue@qq.com
 * @Date: 2021/5/8 10:05 下午
 */
public class BookQueryMSService {
    /**
     * 用于保存图书册数
     */
    private Book[] books;

    /**
     * 用于存储当前存书数量
     */
    private int total;

    /**
     * BookQueryMSService的构造器用于初始化Book数组
     *
     * @param maxCapacityOfBooks 希望保存的最大存书的最大数量
     */
    public BookQueryMSService(int maxCapacityOfBooks) {
        books = new Book[maxCapacityOfBooks];
    }

    /**
     * 将book添加到books数组当中
     * @param book 待添加的书籍
     * @return 添加成功返回true，添加失败抛出异常
     */
    public boolean addBook(Book book) {
        if (total >= books.length) {
            throw new CustomizeException(CustomizeErrorCode.OVER_MAX_CAPACITY_OF_BOOKS);
        }
        //没有异常抛出时向books数组中添加book对象
        books[total++] = book;
        return true;
    }

    /**
     * 删除指定index的book
     *
     * @param index 待删除的book的index
     * @return 删除成功返回true，删除失败抛出异常
     */
    public boolean deleteBook(int index) {
        if (index < 0 || index >= total) {
            throw new CustomizeException(CustomizeErrorCode.BOOK_NOT_FOUND_TO_DELETE);
        }
        //删除指定index上数组位置的元素，通过直接前移覆盖、最后一个置为空实现book的删除
        for (int i = index; i < total - 1; i++) {
            //index的位置就是要删除的book的位置
            books[i] = books[i + 1];
        }
        books[total - 1] = null;
        //更新存储的book数量
        total--;
        return true;
    }

    /**
     * 找到指定index的书，并替换更新
     * @param index 指定的书籍索引
     * @param book 已更新的书籍对象
     * @return 成功返回true，否则抛出异常
     */
    public boolean replaceBook(int index, Book book){
        if(index < 0 || index >= total){
            //index合法性校验
            throw new CustomizeException(CustomizeErrorCode.BOOK_NOT_FOUND_TO_UPDATE);
        }
        books[index] = book;
        return true;
    }

    /**
     * 遍历当前books中保存的书籍
     * @return 返回一个保存了book信息的Book数组
     */
    public Book[] getAllBooks(){
        Book[] b = new Book[total];
        //遍历当前books数组中保存的书籍，并赋值给b
        for (int i = 0; i < total; i++) {
            b[i] = books[i];
        }
        return b;
    }

    /**
     * 根据指定的索引获取书籍
     * @param index 指定的索引位置
     * @return 当找到指定book对象时返回该对象，否则抛出书籍未找到异常
     */
    public Book getBook(int index){
        if(index < 0 || index >= total){
            throw new CustomizeException(CustomizeErrorCode.BOOK_NOT_FOUND);
        }
        return books[index];
    }

    //todo 模糊查询书籍 getBookLike()


    /**
     * 返回books中保存的book对象的数量
     * @return 返回当前books存书的数量
     */
    public int getTotal(){
        return total;
        //不是返回books可用空间的长度，而是返回当前已存书籍的数量
    }

    /**
     * 根据传入的index借出书籍
     * @param index 指定的书籍
     * @return 借出成功返回true，否则抛出异常
     */
    public boolean borrowBook(int index){

        if(index < 0 || index >= total){
            throw new CustomizeException(CustomizeErrorCode.BOOK_NOT_FOUND);
        }
        if(books[index].getCopies() <= 0){
            throw new CustomizeException(CustomizeErrorCode.BOOK_NOT_ENOUGH_TO_BORROW);
        }

        //获取当前书籍的存量
        int copies = books[index].getCopies();

        //将保有数量自减后存入保有数量
        copies--;
        books[index].setCopies(copies);

        return true;
    }

    /**
     * 根据传入的index归还书籍
     * @param index 指定的书籍
     * @return 归还成功返回true，否则抛出异常
     */
    public boolean returnBook(int index){
        if(index < 0 || index >= total){
            throw new CustomizeException(CustomizeErrorCode.BOOK_NOT_FOUND);
        }

        //获取当前书籍的存量
        int copies = books[index].getCopies();

        //将保有数量自增后存入保有数量
        books[index].setCopies(++copies);

        return true;
    }
}
