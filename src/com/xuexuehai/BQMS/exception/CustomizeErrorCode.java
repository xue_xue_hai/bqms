package com.xuexuehai.BQMS.exception;

/**
 * 自定义异常错误代码枚举类
 *
 * @Author: xuexuehai
 * @MailBox: xuehai.xue@qq.com
 * @Date: 2021/5/8 10:44 下午
 */
public enum CustomizeErrorCode implements CustomizeErrorCodeInterface {

    /**
     * 当Books数组中存储的书本数量total超过books数组定义的最大存书量则抛出此异常
     */
    OVER_MAX_CAPACITY_OF_BOOKS(1001, "超出图书查询管理系统的最大存书量"),

    /**
     * 当待删除的书籍未找到时返回此错误
     */
    BOOK_NOT_FOUND_TO_DELETE(1002, "当前想要删除的书目不存在"),

    /**
     * 当查询某个用户时，不存在返回此错误
     */
    BOOK_NOT_FOUND(1003, "找不到当前这本书"),

    /**
     * 当某一本书的copies不足时仍然要借出，返回此错误
     */
    BOOK_NOT_ENOUGH_TO_BORROW(1004, "该书在当前书库中存量不足"),

    /**
     * 当找不到被更新的书时，抛出此异常
     */
    BOOK_NOT_FOUND_TO_UPDATE(1005, "当前想要更新的书目不存在"),

    ;

    private String message;

    private Integer code;

    CustomizeErrorCode(Integer code, String message) {
        this.message = message;
        this.code = code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public Integer getCode() {
        return code;
    }
}
