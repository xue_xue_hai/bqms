package com.xuexuehai.BQMS.exception;

/**
 * 自定义异常类的接口
 *
 * @Author: xuexuehai
 * @MailBox: xuehai.xue@qq.com
 * @Date: 2021/5/8 10:40 下午
 */
public interface CustomizeErrorCodeInterface {
    /**
     * 获取错误信息
     *
     * @return 返回错误信息
     */
    String getMessage();

    /**
     * 获取错误代码
     *
     * @return 返回错误代码
     */
    Integer getCode();
}
