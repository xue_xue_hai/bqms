package com.xuexuehai.BQMS.view;

import com.xuexuehai.BQMS.dao.Book;
import com.xuexuehai.BQMS.exception.CustomizeException;
import com.xuexuehai.BQMS.service.BookQueryMSService;
import com.xuexuehai.BQMS.util.BQMSUtility;

import java.time.LocalDate;
import java.time.ZoneOffset;

/**
 * @Author: xuexuehai
 * @MailBox: xuehai.xue@qq.com
 * @Date: 2021/5/8 10:04 下午
 */
public class BookQueryMSView {
    private BookQueryMSService bookQueryMSService = new BookQueryMSService(20);

    public BookQueryMSView() {
        Book book = new Book("ISBN" + System.currentTimeMillis() + "", "活着", "作家出版社",
                "余华", 35, LocalDate.parse("1993-04-03"), 100, "社科");
        bookQueryMSService.addBook(book);
    }

    /**
     * 程序主菜单界面
     */
    public void enterMainMenu() {
        boolean isFlag = true;
        do {
            System.out.println("-----------------------图书查询管理系统-----------------------");
            System.out.println("                         1 添加图书");
            System.out.println("                         2 删除图书");
            System.out.println("                         3 更新图书");
            System.out.println("                         4 查询图书");
            System.out.println("                         5 借出图书");
            System.out.println("                         6 归还图书");
            System.out.println("                         7 退出系统\n");
            System.out.print("                         请选择(1~7):");

            char menu = BQMSUtility.readMenuSelection(0);

            switch (menu) {
                //可以使用枚举类代替
                case '1':
                    addNewBook();
                    break;
                case '2':
                    deleteBook();
                    break;
                case '3':
                    updateBook();
                    break;
                case '4':
                    search();
                    break;
                case '5':
                    borrowBook();
                    break;
                case '6':
                    returnBook();
                    break;
                case '7':
                    System.out.print("确认是否继续退出(Y/N):");
                    char isExit = BQMSUtility.readConfirmSelection();
                    if (isExit == 'Y') {
                        isFlag = false;
                    }
                    break;
                default:
                    throw new IllegalStateException("Unexpected value: " + menu);
            }

        } while (isFlag);
    }

    /**
     * 添加图书界面
     */
    public void addNewBook() {
        System.out.println("--------------------------添加图书---------------------------");
        System.out.print("书名: ");
        String title = BQMSUtility.readString(20);
        System.out.print("出版社: ");
        String publisher = BQMSUtility.readString(25);
        System.out.print("作者: ");
        String author = BQMSUtility.readString(15);
        System.out.print("价格: ");
        double price = BQMSUtility.readFloat();
        System.out.print("出版时间: ");
        LocalDate date = BQMSUtility.readDate();
        System.out.print("库存数量: ");
        int copies = BQMSUtility.readIntBig();
        System.out.print("书籍分类: ");
        String type = BQMSUtility.readString(50);

        Book book = new Book("ISBN" + LocalDate.now().atStartOfDay().toInstant(ZoneOffset.of("+8")).toEpochMilli(),
                title, publisher, author, price, date, copies, type);
        try {
            boolean isSuccess = bookQueryMSService.addBook(book);
            if (isSuccess) {
                System.out.println("--------------------------添加成功---------------------------");
            }
        } catch (CustomizeException customizeException) {
            System.out.println("----------------------图书已满，添加失败-----------------------");
            customizeException.getMessage();
        }
    }

    /**
     * 删除书籍界面
     */
    public void deleteBook() {
        System.out.println("--------------------------删除书籍---------------------------");
        int number;

        searchBooks();

        //用户输入的待删书籍编号合法性校验
        for (; ; ) {
            System.out.print("请选择待删除的书籍编号(-1退出)：");
            number = BQMSUtility.readInt();

            //退出判断
            if (number == -1) {
                return;
            }

            try {
                //寻找指定编号的书籍
                //用户输入的编号从1开始，数组编号从0开始，故number - 1
                Book book = bookQueryMSService.getBook(number - 1);
                break;
            } catch (CustomizeException customizeException) {
                System.out.println("---------------------当前待删除图书不存在----------------------");
                customizeException.getMessage();
            }
        }
        //找到了指定的书籍
        //误操作保护
        System.out.print("确认是否删除(Y/N):");
        char isDelete = BQMSUtility.readConfirmSelection();
        if (isDelete == 'Y') {
            try {
                //用户输入的编号从1开始，数组编号从0开始，故number - 1
                boolean deleteSuccess = bookQueryMSService.deleteBook(number - 1);
                if (deleteSuccess) {
                    //删除成功提示
                    System.out.println("--------------------------删除成功---------------------------");
                }
            } catch (CustomizeException customizeException) {
                System.out.println("---------------------当前待删除图书不存在----------------------");
                customizeException.getMessage();
            }
        }
    }

    /**
     * 更新图书
     */
    public void updateBook() {
        System.out.println("--------------------------更新图书---------------------------");

        int number;
        Book book;

        searchBooks();

        //用户输入状态确认
        for(;;){
            System.out.print("请选择待更新的书籍(-1退出)");
            number = BQMSUtility.readInt();

            if(number == -1){
                return;
            }

            try {
                //寻找指定编号的书籍
                //用户输入的编号从1开始，数组编号从0开始，故number - 1
                book = bookQueryMSService.getBook(number - 1);
                break;
            } catch (CustomizeException customizeException) {
                System.out.println("---------------------当前待更新图书不存在----------------------");
                customizeException.getMessage();
            }
        }

        //更新书籍信息
        System.out.print("书名: ");
        String title = BQMSUtility.readString(20);
        System.out.print("出版社: ");
        String publisher = BQMSUtility.readString(25);
        System.out.print("作者: ");
        String author = BQMSUtility.readString(15);
        System.out.print("价格: ");
        double price = BQMSUtility.readFloat();
        System.out.print("出版时间: ");
        LocalDate date = BQMSUtility.readDate();
        System.out.print("库存数量: ");
        int copies = BQMSUtility.readIntBig();
        System.out.print("书籍分类: ");
        String type = BQMSUtility.readString(50);

        //将上述更新后的书籍信息装入book类
        book = new Book("ISBN" + LocalDate.now().atStartOfDay().toInstant(ZoneOffset.of("+8")).toEpochMilli(),
                title, publisher, author, price, date, copies, type);

        //通过BookQueryMSService中的replaceBook方法向bookQueryMSService中更新指定书籍，并返回布尔值
        //用户输入的编号从1开始，数组编号从0开始，故number - 1
        try {
            boolean isReplaced = bookQueryMSService.replaceBook(number - 1, book);
            if (isReplaced) {
                System.out.println("--------------------------更新成功---------------------------");
            }
        } catch (CustomizeException customizeException) {
            System.out.println("--------------------------更新失败---------------------------");
            customizeException.getMessage();
        }
    }

    /**
     * 查询书籍界面
     */
    public void search() {
        boolean isFlag = true;
        do {
            System.out.println("-----------------------书籍查询-----------------------");
            System.out.println("                     1 查询所有");
            System.out.println("                     2 模糊查询");
            System.out.println("                     3 退出查询");
            System.out.print("                     请选择(1～3):");

            int menu = BQMSUtility.readMenuSelection(1);

            switch (menu) {
                //可以使用枚举类代替
                case '1':
                    searchBooks();
                    System.out.println("--------------------书籍列表打印完成-------------------");
                    break;
                case '2':
                    searchBook();
                    System.out.println("--------------------查询结果打印完成-------------------");
                    break;
                case '3':
                    System.out.print("确认是否退出查询(Y/N):");
                    char isExit = BQMSUtility.readConfirmSelection();
                    if (isExit == 'Y') {
                        isFlag = false;
                    }
                    break;
                default:
                    throw new IllegalStateException("Unexpected value: " + menu);
            }

        } while (isFlag);

    }

    /**
     * 模糊查询书籍
     */
    private void searchBook() {
        //todo searchBook()
    }

    /**
     * 查询所有书籍
     */
    private void searchBooks() {
        System.out.println("--------------------------书籍列表---------------------------");

        int total = bookQueryMSService.getTotal();
        if (total == 0) {
            System.out.println("没有书籍记录");
        } else {
            System.out.println("编号\tISBN\t\t\t\t书名\t\t出版社\t\t作者\t\t价格\t\t出版日期\t\t库存数量\t书籍类型");
            Book[] books = bookQueryMSService.getAllBooks();
            for (int i = 0; i < books.length; i++) {
                Book book = books[i];
                System.out.println((i + 1) + "\t" + book.getIsbn() +
                        "\t" + book.getTitle() + "\t\t" + book.getPublisher() +
                        "\t" + book.getAuthor() + "\t\t" + book.getPrice() + "\t" + book.getDate() +
                        "\t" + book.getCopies() + "\t\t" + book.getType());
            }
        }


    }

    /**
     * 借书界面
     */
    public void borrowBook() {
        System.out.println("-----------------------图书借阅-----------------------");
        int number;
        searchBooks();

        //用户输入的待借书籍编号合法性校验
        for (; ; ) {
            System.out.print("请选择待借出的书籍编号(-1退出)：");
            number = BQMSUtility.readInt();
            //退出判断
            if (number == -1) {
                return;
            }

            try {
                //寻找指定编号的书籍
                //用户输入的编号从1开始，数组编号从0开始，故number - 1
                Book book = bookQueryMSService.getBook(number - 1);
                break;
            } catch (CustomizeException customizeException) {
                System.out.println("-------------------当前待借出图书不存在-------------------");
                customizeException.getMessage();
            }
        }
        //找到了指定的书籍
        //误操作保护
        System.out.print("确认是否借出(Y/N):");
        char isBorrow = BQMSUtility.readConfirmSelection();
        if (isBorrow == 'Y') {
            try {
                //用户输入的编号从1开始，数组编号从0开始，故number - 1
                boolean borrowSuccess = bookQueryMSService.borrowBook(number - 1);
                if (borrowSuccess) {
                    //借出成功提示
                    System.out.println("-----------------------借出成功-----------------------");
                }
            } catch (CustomizeException customizeException) {
                System.out.println("-------------------当前待借出图书库存不足-------------------");
                customizeException.getMessage();
            }
        }
    }

    /**
     * 还书界面
     */
    public void returnBook() {
        System.out.println("-------------------------图书归还-----------------------");

        int number;

        //用户输入的待还书籍编号合法性校验
        for (; ; ) {
            System.out.print("请输入待归还的书籍编号(-1退出)：");
            number = BQMSUtility.readInt();

            //退出判断
            if (number == -1) {
                return;
            }

            try {
                //寻找指定编号的书籍
                //用户输入的编号从1开始，数组编号从0开始，故number - 1
                Book book = bookQueryMSService.getBook(number - 1);
                break;
            } catch (CustomizeException customizeException) {
                System.out.println("-------------------当前待归还图书不存在-------------------");
                customizeException.getMessage();
            }
        }
        //找到了指定的书籍
        //误操作保护
        System.out.print("确认是否归还(Y/N):");
        char isReturn = BQMSUtility.readConfirmSelection();
        if (isReturn == 'Y') {
            try {
                //用户输入的编号从1开始，数组编号从0开始，故number - 1
                boolean borrowSuccess = bookQueryMSService.returnBook(number - 1);
                if (borrowSuccess) {
                    //归还成功提示
                    System.out.println("-----------------------归还成功-----------------------");
                }
            } catch (CustomizeException customizeException) {
                System.out.println("-------------------当前待归还图书不存在-------------------");
                customizeException.getMessage();
            }
        }
    }

    /**
     * 程序运行主入口
     */
    public static void main(String[] args){
        BookQueryMSView bookQueryMSView = new BookQueryMSView();
        bookQueryMSView.enterMainMenu();
    }
}
