package com.xuexuehai.BQMS.dao;

import lombok.Data;

import java.time.LocalDate;

/**
 * Book实体类，包含Book的属性
 *
 * @Author: xuexuehai
 * @MailBox: xuehai.xue@qq.com
 * @Date: 2021/5/8 9:40 下午
 */
@Data
public class Book {
    /**
     * 书的唯一编号ISBN
     */
    private String isbn;

    /**
     * 书名
     */
    private String title;

    /**
     * 出版社
     */
    private String publisher;

    /**
     * 作者
     */
    private String author;

    /**
     * 价格
     */
    private double price;

    /**
     * 发行日期
     */
    private LocalDate date;

    /**
     * 保有数量
     */
    private int copies;

    /**
     * 书籍类型
     */
    private String type;

    public Book() {
    }

    public Book(String isbn) {
        super();
        this.isbn = isbn;
    }

    public Book(String isbn, String title, String publisher, String author,
                double price, LocalDate date, int copies, String type) {
        this.isbn = isbn;
        this.title = title;
        this.publisher = publisher;
        this.author = author;
        this.price = price;
        this.date = date;
        this.copies = copies;
        this.type = type;
    }
}
